"""Main module for to run C-PAC."""
import logging
import os.path as op

from flywheel_bids.utils import download_run_level, run_level
from flywheel_gear_toolkit.interfaces.command_line import (
    build_command_list,
    exec_command,
)

from fw_gear_cpac import parser, results

log = logging.getLogger(__name__)


def run(gear_context):
    """
    Three part, main method to (1) Get and check BIDS structure, (2) Construct and run
    the cpac command, and (3) Locate and zip the results.
    Args
        gear_context (Geartoolkit context)
    Returns
        Finalized, zipped results or error codes.
    """
    # Fail fast, if the pipeline is not specified
    parser._check_pipeline_specified(gear_context)

    # Gather the background information about the analysis and container
    hierarchy = run_level.get_analysis_run_level_and_hierarchy(
        gear_context.client, gear_context.destination["id"]
    )

    # Add an entry to keep track of where the bids data will live (separate from input/output)
    hierarchy["bids_dir"] = op.join(gear_context.work_dir, "bids")

    _get_and_check_bids(gear_context, hierarchy)
    hierarchy = parser.refine_level_based_inputs(hierarchy)

    # Gather the args and files for the CPAC command
    rc = _make_and_run_command(gear_context, hierarchy)

    results.Zip_Results(gear_context, hierarchy)

    return rc


def _get_and_check_bids(gear_context, hierarchy, folders=["anat", "func", "fmap"]):
    """
    Find and retrieve BIDSified project data.

    Since this is a BIDS app, the inputs must be BIDS-compliant. One way to ensure compliance is to
    import or download the BIDS directory structure. Even though the directory should pass validation,
    CPAC depends on BIDS structure and cannot proceed if validation fails. CPAC validation isn't
    recorded neatly or early in the process, so checking the format here prior to calling the CPAC
    commands is preferable.
    Args
        gear_context (GeartoolkitContext): Gear information
    """
    log.info("Checking BIDS")
    work_dir = op.join(gear_context.work_dir, "bids")
    try:
        download_run_level.download_bids_for_runlevel(
            gear_context,
            hierarchy,
            folders=folders,
            dry_run=gear_context.config.get("gear-dry-run", False),
        )
    except Exception as e:
        log.error(f"Could not download BIDS data from {work_dir}")
        log.exception(e)


def _make_and_run_command(gear_context, hierarchy):
    """
    Parse the commands from the input configuration and package a list of strings that can
    be sent to the shell-based exec_command method.
    Errors are handled internally to exec_command.
    Args
        gear_context (GeartoolkitContext): Gear information
        hierarchy: dictionary containing organizational information about the analysis
    """
    input_files, gear_args = parser.parse_config(gear_context)
    # Instantiate the command string with the script called by C-PAC
    command = ["/usr/local/miniconda/bin/python", "/code/run.py"]
    if input_files:
        for k, v in input_files.items():
            command.extend(["--" + k, v])
    command = build_command_list(command, gear_args)

    # End the command with participant info, as this can be a list in commandline version of C-PAC and,
    # thus, they stick participant on the end
    if gear_context.config["test_config"]:
        lvl = "test_config"
    elif hierarchy["run_level"] == "project":
        lvl = "group"
    else:
        lvl = "participant"

    command.extend(
        [
            hierarchy["bids_dir"],
            hierarchy["input_data_dir"],
            lvl,
        ]
    )

    log_file = op.join(gear_context.output_dir, "cpac_log.txt")
    command.extend(
        [hierarchy["bids_dir"], hierarchy["input_data_dir"], lvl, ">", log_file]
    )
    log.info("CPAC Command-Line: " + " ".join(command))
    # The dry_run variable does not execute command
    # log.debug(environ)
    if gear_context.config.get("gear-dry-run"):
        log.info("Dry run is set")
        exec_command(command, dry_run=True)
        return 0
    else:
        try:
            exec_command(command, dry_run=False)
            return 0
        except Exception as e:
            log.error(e)
            results.report_reprozip(gear_context.output_dir)
            return 1
