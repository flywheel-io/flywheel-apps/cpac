"""Test main.py"""
from unittest.mock import MagicMock, PropertyMock, patch

import pytest
from flywheel_gear_toolkit import GearToolkitContext

from fw_gear_cpac.main import _get_and_check_bids, _make_and_run_command, run


@patch("fw_gear_cpac.main.results.Zip_Results")
@patch("fw_gear_cpac.main._get_and_check_bids")
@patch("fw_gear_cpac.main.parser.refine_level_based_inputs")
@patch("fw_gear_cpac.main._make_and_run_command", return_value=0)
def test_run_with_session_succeeds(
    mock_run, mock_parser, mock_check_bids, mock_zip, mock_context, mock_hierarchy
):
    with patch(
        "fw_gear_cpac.main.run_level.get_analysis_run_level_and_hierarchy",
        return_value=mock_hierarchy,
    ):
        rc = run(mock_context)
    assert mock_zip.call_count == 1
    assert mock_parser.call_count == 1
    assert rc == 0


@pytest.mark.parametrize(
    "dry_run, mock_exec_out, expected",
    [
        (True, [], "Dry run is set"),
        (False, ["stdout_msg", "", 0], "stdout_msg"),
        (False, ["", "std_err", 1], "std_err"),
    ],
)
@patch("fw_gear_cpac.main.exec_command")
@patch(
    "fw_gear_cpac.main.build_command_list",
    return_value=["/code/run.py", "new_gear_args"],
)
@patch(
    "fw_gear_cpac.main.parser.parse_config",
    return_value=(
        {"test_file_1": "my_test_file", "test_file_2": "your_test_file"},
        {"fake_gear_arg1": True, "fake_gear_arg2": False},
    ),
)
def test_run_makes_and_runs_command(
    mock_parse,
    mock_build,
    mock_exec,
    dry_run,
    mock_exec_out,
    expected,
    mock_context,
    mock_hierarchy,
    caplog,
):
    """
    The method parse gear_args, adds the args to the C-PAC run command, and then executes
    the command. Test whether a command is built and the exec_command is called.
    """

    mock_context.config["gear-dry-run"] = dry_run
    mock_exec.return_value = mock_exec_out
    _make_and_run_command(mock_context, mock_hierarchy)
    assert mock_parse.call_count == 1
    assert mock_build.call_count == 1
    assert mock_exec.call_count == 1
    assert any(expected in rec.message for rec in caplog.records)


@patch(
    "fw_gear_cpac.main.download_run_level.download_bids_for_runlevel",
    side_effect=Exception("ERROR"),
)
def test_get_and_check_bids_reports_errors(
    mock_dwnld, mock_context, mock_hierarchy, caplog
):
    _get_and_check_bids(mock_context, mock_hierarchy)
    assert mock_dwnld.called
    assert "ERROR" in caplog.messages[-1]
