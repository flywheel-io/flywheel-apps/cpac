# This Dockerfile constructs a docker image to run C-PAC
#
# Example build:
#   docker build --no-cache --tag flywheel/c-pac `pwd`
#

# Start with the bids/cpac
# Now 4.72GB (March 2021)
# Built on Bionic, using py3.7.13
FROM fcpindi/c-pac:release-v1.8.5

# Note the Maintainer
LABEL MAINTAINER="Flywheel <support@flywheel.io>"

RUN apt-get update && \
    apt-get install -y --no-install-recommends  \
    build-essential  \
    checkinstall  \
    libreadline-gplv2-dev \
	libncursesw5-dev \
    libssl-dev  \
    libsqlite3-dev  \
    tk-dev  \
    libgdbm-dev  \
    libc6-dev \
	libbz2-dev  \
    openssl  \
    libffi-dev \
    gcc \
	tree \
    wget \
	zip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Install python3.9 as the alternate, but don't use deadsnakes as they no longer
# support Bionic
RUN mkdir -p $HOME/opt && \
	cd $HOME/opt && \
	curl -O https://www.python.org/ftp/python/3.9.2/Python-3.9.2.tgz && \
	tar -xzf Python-3.9.2.tgz && \
    cd Python-3.9.2/ && \
    ./configure --enable-shared --enable-optimizations --prefix=/usr/local LDFLAGS="-Wl,--rpath=/usr/local/lib" && \
    make altinstall

# Make directory for flywheel spec (v0)
ENV FLYWHEEL=/flywheel/v0
WORKDIR ${FLYWHEEL}

# Installing alternate pipelines
RUN wget -P /cpac_resources/ https://github.com/FCP-INDI/C-PAC/tree/main/CPAC/resources/configs

# Install BIDS Validator
RUN apt-get update && \
    apt-get install -y --no-install-recommends  \
    gcc \
    nodejs-dev  \
    node-gyp  \
    libssl1.0-dev \
    nodejs \
    npm \
    bash \
    jq && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
# https://www.npmjs.com/package/bids-validator
RUN npm install -g bids-validator@1.9.9

# Installing main dependencies
COPY requirements.txt $FLYWHEEL/
RUN python3.9 -m pip install --upgrade --ignore-installed wheel pip setuptools
RUN python3.9 -m pip install --no-cache-dir --ignore-installed -r $FLYWHEEL/requirements.txt

# Installing the current project (most likely to change, above layer can be cached)
COPY ./ $FLYWHEEL/
RUN python3.9 -m pip install --no-cache-dir .

# Configure entrypoint
RUN chmod a+x $FLYWHEEL/run.py
ENTRYPOINT ["python3.9","/flywheel/v0/run.py"]